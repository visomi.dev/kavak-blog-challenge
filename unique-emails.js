const input = [
  'test.email+alex@kavak.com',
  'test.e.mail+bob.cathy@kavak.com',
  'testemail+david@ka.vak.com'
]

const output = ['testemail@kavak.com', 'testemail@ka.vak.com']

const uniqueEmails = emails => emails.reduce((accum, email) => {
  const [user, domain] = email.split('@')

  const [unique] = user.replace(/\./g, '').split(/[+]/)

  const forTest = `${unique}@${domain}`;

  if (!accum.includes(forTest)) accum.push(forTest)

  return accum
}, [])

const result = uniqueEmails(input)
console.log(result);
console.log('Equal:', `${result}` === `${output}`)
