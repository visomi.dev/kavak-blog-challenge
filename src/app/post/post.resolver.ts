import { Observable } from 'rxjs';

import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

import { PostService, IPost } from './post.service';

@Injectable()
export class PostResolver implements Resolve<any> {
  constructor(private postService: PostService) {}

  resolve(): Observable<IPost[]> {
    return this.postService.getPosts();
  }
}
