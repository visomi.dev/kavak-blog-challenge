import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

const capitalize = (word: string) => word[0].toUpperCase() + word.substr(1).toLowerCase();

export interface IPost {
  userId: number;
  id: number;
  title: string;
  body: string;
}

@Injectable()
export class PostService {
  endpoint = 'https://jsonplaceholder.typicode.com/posts';

  constructor(private http: HttpClient) {}

  capitalizePost(post: IPost): IPost {
    post.title = post.title.split(' ').map(capitalize).join(' ');

    return post;
  }

  getPosts(): Observable<IPost[]> {
    return this.http.get<IPost[]>(this.endpoint).pipe(map(d => d.map(this.capitalizePost)));
  }
}
