import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPost } from './post.service';
import { FormGroup, FormControl } from '@angular/forms';


@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  posts: IPost[];
  search = new FormControl('');

  constructor(private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.posts = this.route.snapshot.data.posts;
  }

  get filteredPosts(): IPost[] {
    const filter = this.search.value;

    return this.posts.filter(({ title }) => title.includes(filter));
  }

  get link(): (id: number) => {} {
    return (id: number) => `/post-detail/${id}`;
  }
}
