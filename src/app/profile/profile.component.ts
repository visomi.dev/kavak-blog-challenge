import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from '../shared/user.service';
import { MessageService } from '../shared/message.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  loading = false;

  profileForm = new FormGroup({
    email: new FormControl('', [
      Validators.required,
      Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$'),
    ]),
    phoneNumber: new FormControl('+(52)', [
      Validators.required,
      Validators.pattern('^([+][(][0-9]{1,4}[)])([0-9]{5,})$'),
    ]),
    firstName: new FormControl('', [
      Validators.required,
      Validators.pattern('^([A-Za-z]){3,120}$'),
    ]),
    lastName: new FormControl('', [
      Validators.required,
      Validators.pattern('^([A-Za-z]){3,120}$'),
    ]),
    identificationNumber: new FormControl('', [
      Validators.required,
      Validators.pattern('^[0-9]{1,120}$'),
    ]),
    password: new FormControl('', [
      Validators.required,
      Validators.pattern('(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,50}')
    ]),
    passwordConfirmation: new FormControl('', [
      Validators.required,
    ]),
  }, [this.checkPassword]);

  checkPassword(formGroup: FormGroup): null | { notSamePassword: boolean } {
    const password = formGroup.get('password').value;
    const passwordConfirmation = formGroup.get('passwordConfirmation').value;

    return password === passwordConfirmation ? null : { notSamePassword: true };
  }

  constructor(
    private userService: UserService,
    private messageService: MessageService,
    private router: Router
  ) {}

  get isInvalid(): (formControl: string) => {} {
    return (formControl: string): boolean => {
      return this.profileForm.get(formControl).invalid && this.profileForm.get(formControl).touched;
    };
  }

  get isInvalidPassword(): boolean {
    return this.profileForm.hasError('notSamePassword') && this.profileForm.get('passwordConfirmation').touched;
  }

  async onUpdate(): Promise<void> {
    this.loading = true;

    try {
      await this.userService.update(this.userService.user.id, {
        email: this.profileForm.get('email').value,
        phoneNumber: this.profileForm.get('phoneNumber').value,
        firstName: this.profileForm.get('firstName').value,
        lastName: this.profileForm.get('lastName').value,
        identificationNumber: this.profileForm.get('identificationNumber').value,
        password: this.profileForm.get('password').value,
      });

      this.messageService.addTemporalMessage({
        type: 'alert-success',
        message: 'Usuario actualizado'
      });
  } catch ({ message }) {
      if (message === 'user-exists') {
        this.messageService.addTemporalMessage({
          type: 'alert-danger',
          message: 'El email proporcionado ya existe'
        });
      }
    }

    this.loading = false;
  }

  async ngOnInit(): Promise<void> {
    const user = await this.userService.getUser(this.userService.user.id);

    this.profileForm.get('email').setValue(user.email);
    this.profileForm.get('phoneNumber').setValue(user.phoneNumber);
    this.profileForm.get('firstName').setValue(user.firstName);
    this.profileForm.get('lastName').setValue(user.lastName);
    this.profileForm.get('identificationNumber').setValue(user.identificationNumber);
    this.profileForm.get('password').setValue(user.password);
    this.profileForm.get('passwordConfirmation').setValue(user.password);
  }
}
