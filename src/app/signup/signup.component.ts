import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { UserService } from '../shared/user.service';
import { MessageService } from '../shared/message.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent {
  loading = false;

  signupForm = new FormGroup({
    email: new FormControl('', [
      Validators.required,
      Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$'),
    ]),
    phoneNumber: new FormControl('+(52)', [
      Validators.required,
      Validators.pattern('^([+][(][0-9]{1,4}[)])([0-9]{5,})$'),
    ]),
    firstName: new FormControl('', [
      Validators.required,
      Validators.pattern('^([A-Za-z]){3,120}$'),
    ]),
    lastName: new FormControl('', [
      Validators.required,
      Validators.pattern('^([A-Za-z]){3,120}$'),
    ]),
    identificationNumber: new FormControl('', [
      Validators.required,
      Validators.pattern('^[0-9]{1,120}$'),
    ]),
    password: new FormControl('', [
      Validators.required,
      Validators.pattern('(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,50}')
    ]),
    passwordConfirmation: new FormControl('', [
      Validators.required,
    ]),
  }, [this.checkPassword]);

  checkPassword(formGroup: FormGroup): null | { notSamePassword: boolean } {
    const password = formGroup.get('password').value;
    const passwordConfirmation = formGroup.get('passwordConfirmation').value;

    return password === passwordConfirmation ? null : { notSamePassword: true };
  }

  constructor(
    private userService: UserService,
    private messageService: MessageService,
    private router: Router
  ) {}

  get isInvalid(): (formControl: string) => {} {
    return (formControl: string): boolean => {
      return this.signupForm.get(formControl).invalid && this.signupForm.get(formControl).touched;
    };
  }

  get isInvalidPassword(): boolean {
    return this.signupForm.hasError('notSamePassword') && this.signupForm.get('passwordConfirmation').touched;
  }

  async onSignup(): Promise<void> {
    this.loading = true;

    try {
      await this.userService.signup({
        email: this.signupForm.get('email').value,
        phoneNumber: this.signupForm.get('phoneNumber').value,
        firstName: this.signupForm.get('firstName').value,
        lastName: this.signupForm.get('lastName').value,
        identificationNumber: this.signupForm.get('identificationNumber').value,
        password: this.signupForm.get('password').value,
      });

      this.router.navigate(['/posts']);
    } catch ({ message }) {
      if (message === 'user-exists') {
        this.messageService.addTemporalMessage({
          type: 'alert-danger',
          message: 'El email proporcionado ya existe'
        });
      }
    }

    this.loading = false;
  }
}
