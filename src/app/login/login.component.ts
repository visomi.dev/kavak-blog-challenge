import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { UserService } from '../shared/user.service';
import { MessageService } from '../shared/message.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  loading = false;

  loginForm = new FormGroup({
    email: new FormControl('', [
      Validators.required,
      Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$'),
    ]),
    password: new FormControl('', [
      Validators.required,
      Validators.pattern('(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,50}')
    ]),
  });

  constructor(
    private userService: UserService,
    private messageService: MessageService,
    private router: Router
  ) {}

  get isInvalid(): (formControl: string) => {} {
    return (formControl: string): boolean => {
      return this.loginForm.get(formControl).invalid && this.loginForm.get(formControl).touched;
    };
  }

  async onLogin(): Promise<void> {
    try {
      this.loading = true;

      await this.userService.login(
        this.loginForm.get('email').value,
        this.loginForm.get('password').value
      );

      this.router.navigate(['/post']);
    } catch ({ message }) {
      if (message === 'user-not-exists') {
        this.messageService.addTemporalMessage({
          type: 'alert-danger',
          message: 'El email proporcionado no existe'
        });
      }

      if (message === 'invalid-password') {
        this.messageService.addTemporalMessage({
          type: 'alert-danger',
          message: 'Contraseña incorrecta'
        });
      }
    }

    this.loading = false;
  }
}
