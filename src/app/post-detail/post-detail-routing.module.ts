import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PostDetailComponent } from './post-detail.component';
import { PostDetailResolver } from './post-detail.resolver';


const routes: Routes = [
  {
    path: ':id',
    component: PostDetailComponent,
    resolve: { post: PostDetailResolver }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PostDetailRoutingModule { }
