import { Observable } from 'rxjs';

import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';

import { PostDetailService, IPost } from './post-detail.service';

@Injectable()
export class PostDetailResolver implements Resolve<any> {
  constructor(private postDetailService: PostDetailService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IPost> {
    return this.postDetailService.getPost(route.paramMap.get('id'));
  }
}
