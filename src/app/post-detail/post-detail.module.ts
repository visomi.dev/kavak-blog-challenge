import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { PostDetailRoutingModule } from './post-detail-routing.module';
import { PostDetailComponent } from './post-detail.component';
import { PostDetailService } from './post-detail.service';
import { PostDetailResolver } from './post-detail.resolver';


@NgModule({
  declarations: [PostDetailComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    PostDetailRoutingModule
  ],
  providers: [
    PostDetailService,
    PostDetailResolver
  ]
})
export class PostDetailModule { }
