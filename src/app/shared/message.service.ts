import { Injectable } from '@angular/core';

const MESSAGE_DEFAULT_DURATION = 5000;

export interface IMessage {
  type: string;
  message: string;
  duration?: number;
  date?: Date;
}

@Injectable({
  providedIn: 'root',
})
export class MessageService {
  messages: IMessage[] = [];

  add(message: IMessage): void {
    if (!message.date) { message.date = new Date(); }

    this.messages.push(message);
  }

  delete(date: Date): void {
    this.messages = this.messages.filter((m) => m.date !== date);
  }

  addTemporalMessage(message: IMessage): void {
    if (!message.date) { message.date = new Date(); }
    if (!message.duration) { message.duration = MESSAGE_DEFAULT_DURATION; }

    this.messages.push(message);

    setTimeout(() => { this.delete(message.date); }, message.duration);
  }

  clear(): void {
    this.messages = [];
  }
}
