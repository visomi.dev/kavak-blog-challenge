import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {
  constructor(
    public router: Router,
    public userService: UserService
  ) { }

  onLogout(): void {
    this.userService.logout();
    this.router.navigate(['/login']);
  }
}
