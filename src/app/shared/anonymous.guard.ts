import { Injectable } from '@angular/core';
import {
  CanActivate,
  CanActivateChild,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router,
} from '@angular/router';

import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class AnonymousGuard implements CanActivate, CanActivateChild {
  canActivate = this.guard;
  canActivateChild = this.guard;

  constructor(private router: Router, private userService: UserService) { }

  guard(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const authed = this.userService.authed;

    if (authed) { this.router.navigate(['/post']); }

    return !authed;
  }
}
