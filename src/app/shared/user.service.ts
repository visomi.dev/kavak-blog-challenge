import { Injectable } from '@angular/core';

import { db, IUser } from '../../db';
import { InnerSubscriber } from 'rxjs/internal/InnerSubscriber';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  authed = false;
  user: IUser;

  async login(email: string, password: string): Promise<IUser> {
    const user = await db.users.get({ email });

    if (!user) {
      throw new Error('user-not-exists');
    }

    if (user.password !== password) {
      throw new Error('invalid-password');
    }

    this.authed = true;
    this.user = user;

    return user;
  }

  async getUser(id: number): Promise<IUser> {
    const user = await db.users.get(id);

    if (!user) {
      throw new Error('user-not-exists');
    }

    this.user = user;

    return user;
  }

  async update(id: number, data: IUser): Promise<IUser> {
    await db.users.update(id, data);

    data.id = id;

    this.user = data;

    return data;
  }

  async signup(user: IUser): Promise<IUser> {
    const userExists = await db.users.get({ email: user.email });

    if (userExists) {
      throw new Error('user-exists');
    }

    const id = await db.users.add(user);

    user.id = id;

    this.authed = true;
    this.user = user;

    return user;
  }

  logout(): void {
    this.authed = false;
    this.user = {
      email: '',
      firstName: '',
      identificationNumber: '',
      lastName: '',
      password: '',
      phoneNumber: '',
      id: undefined
    };
  }
}
