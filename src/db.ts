import Dexie from 'dexie';

class DB extends Dexie {
  users: Dexie.Table<IUser, number>;

  constructor() {
    super('KAVAK');

    this.version(2).stores({
      users: '++id, email'
    });
  }
}

export interface IUser {
  id?: number;
  email: string;
  phoneNumber: string;
  firstName: string;
  lastName: string;
  identificationNumber: string;
  password: string;
}

export const db = new DB();
